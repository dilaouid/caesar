# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/30 23:08:41 by dlaouid           #+#    #+#              #
#    Updated: 2019/01/23 16:40:29 by dilaouid         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

PATH_LIB =	./src/lib/
PATH_NLIB =	./src/nlib/
PATH_INC =	./includes/
FLAG =	-Wall -Wextra -Werror -fstack-protector -g -O0 -std=c99
OPTION =	ulimit -c unlimited
SRC =	$(PATH_NLIB)*.c $(PATH_LIB)*.c \
		main.c

all:
	gcc -o caesar $(FLAG) $(SRC)
