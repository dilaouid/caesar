Caesar v.04
===================================================================================

 Si vous ne savez pas ce qu'est le code de César, je vous invite à jeter un oeil
 (désolé, vous voilà borgne, ou aveugle) ici : https://www.dcode.fr/chiffre-cesar
 ________________________
UPDATE V.02
 * Ajout d'un man (utilisez ./a.out -m pour le lire)
 * Ajout d'un décodeur (utilise le système par récurrence) :
        Entrez un message codé, et la lettre la plus récurrente dans la langue originelle du code. Par exemple, c'est le 'e' pour le français. Ce qui donnera 15% de chances de déchiffrer le message sans passer par le méthode bruteforce.
 * Création d'un environnement stable.
________________________
UPDATE V.03
 * Ajout d'un bruteforce (utilisez ./a.out -b "votre texte" pour le lancer)
________________________
UPDATE V.04
 * Possibilité de coder un fichier entré en argument avec le flag -f (utilisez ./a.out -f "votre fichier" "votre clé" pour cette option)
________________________
