/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libcaesar.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/30 20:18:59 by dlaouid           #+#    #+#             */
/*   Updated: 2018/10/31 00:31:13 by dlaouid          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _LIBCAESAR_H
# define _LIBCAESAR_H

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define COLOR_RED "\x1b[31m"
#define COLOR_GREEN "\x1b[32m"
#define COLOR_YELLOW "\x1b[33m"
#define COLOR_BLUE "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN "\x1b[36m"
#define COLOR_RESET "\x1b[0m"

void	ft_putchar(char c);
void	ft_putstr(char *str);
void	ft_putstr_fd(int fd, char *str);
void	ft_putendl(char *str);
void	ft_putendl_fd(int fd, char *str);
void	ft_putnbr(int nb);
int		ft_strlen(char *str);
void	ft_decode(char *msg, char c);
char	*ft_strlowercase(char *str);
void	ft_occur(char *str, int *max, int *tab);
long	ft_atoi(char *str);
int 	ft_islowercase(char c);
int		ft_isuppercase(char c);
void	ft_instructions();
void	ft_instructiondecode(void);
void	ft_man(void);
void	ft_rtfm(void);
int		ft_flag(char *str);
void	ft_bruteforce(char *str);
char	*ft_strdup(char *src);
long	ft_atoi(char *str);
char	*ft_cesar(char *str, int key);
int		ft_get_textfile(char *file, int key);

#endif
