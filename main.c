/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/28 14:42:10 by dlaouid           #+#    #+#             */
/*   Updated: 2018/10/31 00:31:46 by dlaouid          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./includes/libcaesar.h"

int		main(int argc, char **argv)
{
	int uncrypt;
	int key;
	
	ft_instructions();	
	if (argc == 3 && ft_atoi(argv[2]) > 0 && ft_flag(argv[1]) == 0)
	{
		ft_rtfm();
		key = ft_atoi(argv[2]);
		uncrypt = 26;
		ft_putstr("THE SOURCE MESSAGE IS : "COLOR_BLUE);
		ft_putendl(argv[1]);
		ft_putstr(COLOR_RESET);
		ft_putstr("THE KEY IS : "COLOR_BLUE);
		ft_putnbr(key);
		ft_putstr(COLOR_RESET);
		if (key > 26)
		{
			ft_putchar('\n');
			ft_putstr("THE CORRECTED KEY IS : "COLOR_MAGENTA);
			while (key > 26)
				key -= 26;
			ft_putnbr(key);
			ft_putstr(COLOR_RESET);
		}
		ft_putendl("\n----------");
		ft_putendl(COLOR_GREEN "ENCRYPTION SUCCESS" COLOR_RESET);
		ft_putendl("----------");
		ft_putstr("THE CRYPTED MESSAGE IS : " COLOR_YELLOW);
		ft_putstr(ft_cesar(argv[1], key));
		ft_putendl(COLOR_RESET);
		ft_putstr("YOUR UNLOCK KEY IS : "COLOR_YELLOW);
		while (key - uncrypt > 26)
        		key -= 26;
		ft_putnbr(uncrypt - key);
		ft_putendl(COLOR_RESET);
	}
	else if (argc == 4 && ft_flag(argv[1]) == 1 && ft_islowercase(argv[3][0]) == 1)
	{
		ft_rtfm();
		ft_instructiondecode();
		ft_putstr("THE SOURCE MESSAGE IS : "COLOR_BLUE);
		ft_putendl(argv[2]);
		ft_putstr(COLOR_RESET);
		ft_putstr("THE RECCURENCY LETTER IS : "COLOR_BLUE);
		ft_putchar(argv[3][0]);
		ft_putendl(COLOR_RESET"\n----------");
		ft_decode(argv[2], argv[3][0]);
	}
	else if (ft_flag(argv[1]) == 2)
		ft_man();
	else if (argc == 3 && ft_flag(argv[1]) == 3)
		ft_bruteforce(argv[2]);
	else if (argc == 4 && ft_flag(argv[1]) == 4)
		ft_get_textfile(argv[2], ft_atoi(argv[3]));
	else
	{
		ft_rtfm();
		ft_putendl(COLOR_RED"SYNTAX ERROR\n");
	}
	return (0);
}
