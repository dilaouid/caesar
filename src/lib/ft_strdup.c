/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/30 23:36:11 by dlaouid           #+#    #+#             */
/*   Updated: 2018/10/30 23:36:12 by dlaouid          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libcaesar.h"

char	*ft_strdup(char *src)
{
	int i;
	char *dest;	

	i = 0;
	dest = (char *)malloc(sizeof(char) * ft_strlen(src) + 1);
	if (dest == NULL)
		return (NULL);
	while (src[i])
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}