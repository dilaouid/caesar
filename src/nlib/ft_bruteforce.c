/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bruteforce.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/31 00:24:36 by dlaouid           #+#    #+#             */
/*   Updated: 2018/10/31 00:28:59 by dlaouid          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libcaesar.h"

void	ft_bruteforce(char *str)
{
	int i = 1;

	ft_putendl(COLOR_RED"TIME FOR A MASSACRE!!"COLOR_RESET);
	while (i++ <= 26)
	{
		ft_putstr(COLOR_BLUE"TRY N° ");
		ft_putnbr(i);
		ft_putstr(" "COLOR_RESET);
		ft_putendl(ft_cesar(str, i));
		ft_putstr(COLOR_RESET);
	}
}
