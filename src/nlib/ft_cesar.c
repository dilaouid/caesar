/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cesar.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/30 23:43:09 by dlaouid           #+#    #+#             */
/*   Updated: 2018/10/30 23:43:11 by dlaouid          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libcaesar.h"

char	*ft_cesar(char *str, int key)
{
	int i = 0;
	char *code = ft_strdup(str);

	while (code[i++])
	{
		if (ft_isuppercase(code[i]) == 1)
		{
			code[i] += key;
			while (ft_isuppercase(code[i]) == 0)
				code[i] -= 26;
		}
		else if (ft_islowercase(code[i]) == 1)
		{
			code[i] += key;
			while (ft_islowercase(code[i]) == 0)
				code[i] -= 26;
		}
	}
	return (code);
}
