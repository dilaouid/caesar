/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_decode.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/30 19:37:56 by dlaouid           #+#    #+#             */
/*   Updated: 2018/10/30 22:47:16 by dlaouid          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libcaesar.h"

void	ft_decode(char *msg, char recurrency)
{
	char	frequent_char;
	int	possible_key = 0;
	int	max = 0;
	int	alphabet[26];
	int	i = 0;
	int j = 0;

	msg = ft_strlowercase(msg);
	ft_occur(msg, &max, alphabet);
	while (alphabet[i] != max)
		i++;
	frequent_char = i + 96;
	while ((possible_key + frequent_char) != recurrency && (possible_key + frequent_char) <= 'z')
		possible_key++;
	if ((possible_key + frequent_char > 'z'))
	{
		while ((possible_key + frequent_char) != recurrency)
			possible_key--;
	}
	while (msg[j++])
	{
		if (ft_islowercase(msg[j]) == 1)
		{
			msg[j] += possible_key;
			while (ft_islowercase(msg[j]) == 0)
				msg[j] += 26;
		}
	}
	msg[j] = '\0';
	ft_putstr("THE POSSIBLE KEY USED IS : "COLOR_YELLOW);
	ft_putnbr(possible_key);
	ft_putstr(COLOR_RESET"\n");
	ft_putstr("THE POSSIBLE MESSAGE IS : "COLOR_YELLOW);
	ft_putendl(msg);
	ft_putstr(COLOR_RESET);
}
