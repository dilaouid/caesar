/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_file.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/30 23:44:14 by dlaouid           #+#    #+#             */
/*   Updated: 2019/01/23 16:40:01 by dilaouid         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libcaesar.h"

char	*ft_encrypted_flag(char *str, int i)
{
	str + i = ft_strdup("_ENCRYPTED");
	return (str);
}

int		ft_get_textfile(char *file, int key)
{
	int fd;
	int nchar;
	int i = 0;
	char *filename;
	char str[4097];

	fd = open(file, O_RDONLY);
	if (fd == -1)
		return (write(2, "open() failed\n", 14));
	while ((nchar = read(fd, str, 4096)))
		str[4096] = '\0';
	close(fd);
	if (fd == -1)
		return (write(2, "close() failed\n", 15));
	filename = (char *)malloc(sizeof(char) * ft_strlen(file) + 12);
	if (filename == NULL)
		return (0);
	while (file[i++])
		filename[i] = file[i];
	ft_encrypted_flag(filename, i);
	fd = open(filename, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
	if (fd < 0)
		return (write(2, "open() failed\n", 14));
	ft_putendl_fd(fd, ft_cesar(str, key));
	close(fd);
	if (fd < 0)
		return (write(2, "close() failed\n", 15));
	ft_putendl("\n----------");
	ft_putendl(COLOR_GREEN "FILE CREATION SUCCESS" COLOR_RESET);
	ft_putendl("----------");
	ft_putstr(COLOR_GREEN "FILENAME: " COLOR_RESET);
	ft_putendl(filename);
	ft_putstr(COLOR_GREEN "KEY USED : " COLOR_RESET);
	ft_putnbr(key);
	ft_putstr("\n\n");
	return (1);
}
