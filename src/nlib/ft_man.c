/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instructiondecode.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/30 22:34:14 by dlaouid           #+#    #+#             */
/*   Updated: 2018/10/30 22:34:39 by dlaouid          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libcaesar.h"

void	ft_instructions(void)
{
	ft_putstr("\e[1;1H\e[2J");
	ft_putendl("#####################################################################################################"COLOR_GREEN);
	ft_putendl("/////////////////////////////////////////////////////////////////////////////////////////////////////"COLOR_CYAN);
	ft_putendl("In cryptography, a Caesar cipher, also known as Caesar's cipher, the shift cipher, Caesar's code\nor Caesar shift, is one of the simplest and most widely known encryption techniques. It is a type\nof substitution cipher in which each letter in the plaintext is replaced by a letter some fixed\nnumber of positions down the alphabet. For example, with a left shift of 3, D would be replaced by A,\nE would become B, and so on. The method is named after Julius Caesar, who used\nit in his private correspondence.");
	ft_putendl(COLOR_GREEN"/////////////////////////////////////////////////////////////////////////////////////////////////////"COLOR_RESET);
	ft_putendl("#####################################################################################################\n");
}

void	ft_instructiondecode(void)
{
	ft_putstr(COLOR_RED"-----------------------------------------------------------------------------------------------------\nYou are using the decryption method.\nPlease remove the -d flag for encrypt a message with a choosen key.\n-----------------------------------------------------------------------------------------------------\n");
	ft_putstr("For using it, please launch the program like this : ./cesar -d 'your text' 'c' (c is most recurrent\nletter in your language : 'e' is the most reccurent letter in the english language)\n-----------------------------------------------------------------------------------------------------\n\n"COLOR_RESET);
}

void	ft_man(void)
{
	ft_putstr(COLOR_RED"-----------------------------------------------------------------------------------------------------\nYou are reading the manual of the Caesar Code\n-----------------------------------------------------------------------------------------------------\n\nLaunch the program like this for using the code with a choosen key:\n./program_name 'your message' 'your key'\n\n-----------------------------------------------------------------------------------------------------\nLaunch the program like this for using the reccurency method and discover a hidden message!\n./program_name -d 'the strange message' 'c'\n\n(c is most recurrent letter in your language : for example, 'e' is the most reccurent letter in the\nenglish language)\n-----------------------------------------------------------------------------------------------------\n");
	ft_putstr("\nLaunch the program like this for a bruteforce:\n./program_name -b 'your message'\n\n-----------------------------------------------------------------------------------------------------\n\nLaunch the program like this for a coding a text file :\n./program_name -f 'your file' 'your key'\n\n-----------------------------------------------------------------------------------------------------\n");
	ft_putstr("Have fun with my program, pal !\n-----------------------------------------------------------------------------------------------------\n\n"COLOR_RESET);
}

void	ft_rtfm(void)
{
	ft_putstr(COLOR_RED"-----------------------------------------------------------------------------------------------------\nENJOY ! \nUse -m for reading the man !\n-----------------------------------------------------------------------------------------------------\n\n"COLOR_RESET);
}
