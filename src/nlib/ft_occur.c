/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_occur.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dlaouid <dilaouid@42.fr>                   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/30 18:57:04 by dlaouid           #+#    #+#             */
/*   Updated: 2018/10/30 23:10:21 by dlaouid          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libcaesar.h"

void	ft_occur(char *str, int *max, int *tab)
{
	int i = 0;
	int	j = 0;

	while (i <= 26)
		tab[i++] = 0;
	i = 0;
	while (str[j++])
	{
		if (ft_islowercase(str[j]) == 1)
			tab[str[j] - 96] += 1;
	}
	i = 0;
	while (i++ < 26)
	{
		if (tab[i] > *max)
			*max = tab[i];
	}
}
